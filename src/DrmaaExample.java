import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.ggf.drmaa.DrmCommunicationException;
import org.ggf.drmaa.DrmaaException;
import org.ggf.drmaa.JobInfo;
import org.ggf.drmaa.JobTemplate;
import org.ggf.drmaa.Session;
import org.ggf.drmaa.SessionFactory;
import org.ggf.drmaa.FileTransferMode;
import org.ggf.drmaa.Version;

/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

public class DrmaaExample {
	private static int NBULKS = 1;
	private static int JOB_CHUNK = 4;
	private static Session session = null;

	public static void main(String[] args) throws Exception {
		String jobPath = args[0];

		System.out.println("Running script: " + jobPath );

		SessionFactory factory = SessionFactory.getFactory();

		session = factory.getSession();

		System.out.println("\n === Session Info === ");		
		printSessionInfo(session); 

		session.init(null);

		JobTemplate jt = createJobTemplate(jobPath, 10, true);

		List allJobIds = new LinkedList();
		List jobIds = null;
		boolean retry = true;

		System.out.println("\n === Submit Bulk Jobs === "); 
		for (int count = 0; count < NBULKS; count++) {
			retry = true;
			do {
				try {
					jobIds = session.runBulkJobs(jt, 1, JOB_CHUNK, 1);
					retry = false;
					allJobIds.addAll(jobIds);
				} catch (DrmCommunicationException e) {
					System.err.println("runBulkJobs() failed - retry: " + e.getMessage());

					Thread.sleep(1000);
				}
			}
			while (retry);

			System.out.println("submitted bulk job with jobids:");

			Iterator i = jobIds.iterator();

			while (i.hasNext()) {
				System.out.println("\t \"" + i.next() + "\"");
			}
		}

		session.deleteJobTemplate(jt);

		System.out.println("\n === Submit Sequential Jobs === "); 
		/* submit some sequential jobs */
		jt = createJobTemplate(jobPath, 7, false);

		String jobId = null;

		for (int count = 0; count < JOB_CHUNK; count++) {
			retry = true;
			while(retry) {
				try {
					jobId = session.runJob(jt);
					retry = false;
					// NOTE: sleep 1s to ensure torque accepts and increments the job id. 
					Thread.sleep(1000);
				} catch (DrmCommunicationException e) {
					System.err.println("runJob() failed - retry: " + e.getMessage());
					Thread.sleep(1000);
				}
			}

			System.out.println("\t \"" + jobId + "\"");
			allJobIds.add(jobId);
		}

		session.deleteJobTemplate(jt);

		try { 
			Thread.sleep(2000); 
		} catch (InterruptedException e) {
			// Don't care
		} 
	
		System.out.println("\n === Job Status Check === "); 
		Iterator id = allJobIds.iterator();
		while (id.hasNext()) { 
			String sJobId = (String)id.next();
			int progstatus = session.getJobProgramStatus(sJobId);
			System.out.print("\t" + sJobId + " ");

			switch (progstatus) {
				case Session.UNDETERMINED:
					System.out.println("Job status cannot be determined");
					break;
				case Session.QUEUED_ACTIVE:
					System.out.println("Job is queued and active");
					break;
				case Session.SYSTEM_ON_HOLD:
					System.out.println("Job is queued and in system hold");
					break;
				case Session.USER_ON_HOLD:
					System.out.println("Job is queued and in user hold");
					break;
				case Session.USER_SYSTEM_ON_HOLD:
					System.out.println("Job is queued and in user and system hold");
					break;
				case Session.RUNNING:
					System.out.println("Job is running");
					break;
				case Session.SYSTEM_SUSPENDED:
					System.out.println("Job is system suspended");
					break;
				case Session.USER_SUSPENDED:
					System.out.println("Job is user suspended\n");
					break;
				case Session.USER_SYSTEM_SUSPENDED:
					System.out.println("Job is user and system suspended\n");
					break;
				case Session.DONE:
					System.out.println("Job finished normally\n");
					break;
				case Session.FAILED:
					System.out.println("Job finished, but failed\n");
					break;
			} /* switch */
		}

		/* synchronize with all jobs */
		System.out.println("\n === Wait for Completion === "); 
		session.synchronize(allJobIds, Session.TIMEOUT_WAIT_FOREVER, false);
		//session.synchronize(allJobIds, Session.TIMEOUT_NO_WAIT, true);
		// session.synchronize(allJobIds, TIMEOUT_WAIT_FOREVER, true);
		System.out.println("synchronized with all jobs");

		System.out.println("\n === Job Exit Summary === "); 
		/* wait all those jobs */
		Iterator ii = allJobIds.iterator();

		while (ii.hasNext()) {
			JobInfo info = null;
			jobId = (String)ii.next();

			//status = session.wait(jobId, Session.TIMEOUT_NO_WAIT);
			info = session.wait(jobId, Session.TIMEOUT_WAIT_FOREVER);
			// Interrogate job exit status
			if (info.wasAborted ()) {
				System.out.println("Job " + info.getJobId () + " was aborted or failed to run");
			}	
			else if (info.hasSignaled ()) {
				System.out.println("Job " + info.getJobId () +
						" exited due to signal " +
						info.getTerminatingSignal () + 
						" (Exit Status: " + info.getExitStatus () + ")");

				if (info.hasCoreDump()) {
					System.out.println("A core dump is available.");
				}
			}
			else if (info.hasExited ()) {
				System.out.println("Job " + info.getJobId () +
						" finished regularly with exit status " +
						info.getExitStatus ());
			}
			else {
				System.out.println("Job " + info.getJobId () +
						" finished with unclear conditions: " + info.getExitStatus());
			}

			System.out.println ("-- Job Usage --");

			Map rmap = info.getResourceUsage ();
			Iterator iii = rmap.keySet().iterator ();

			while (iii.hasNext ()) {
				String name = (String)iii.next ();
				String value = (String)rmap.get (name);

				System.out.println("  " + name + "=" + value);
			} 
			System.out.println();
		}
	}

	private static void printSessionInfo(Session session) {
		try {
			System.out.println("Supported contact strings: \"" +
					session.getContact() + "\"");
			System.out.println("Supported DRM systems: \"" +
					session.getDrmSystem() + "\"");
			System.out.println("Supported DRMAA implementations: \"" +
					session.getDrmaaImplementation() + "\"");

			session.init("");

			System.out.println("Using contact strings: \"" +
					session.getContact() + "\"");
			System.out.println("Using DRM systems: \"" +
					session.getDrmSystem() + "\"");
			System.out.println("Using DRMAA implementations: \"" +
					session.getDrmaaImplementation() + "\"");

			Version version = session.getVersion();

			System.out.println("Using DRMAA version " + version.toString());

			session.exit();
		} catch (DrmaaException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	private static JobTemplate createJobTemplate(String jobPath, int seconds, boolean isBulkJob) throws DrmaaException {
		JobTemplate jt = session.createJobTemplate();

		jt.setRemoteCommand ("sh sleeper.sh");
		jt.setArgs(Collections.singletonList(Integer.toString(seconds)));
		jt.setWorkingDirectory("/home/testuser");
		jt.setJobName("DRMAA_JOB"); 
		jt.setJoinFiles(true);
		//jt.setSoftWallclockTimeLimit(3600);
		//jt.setSoftDurationTimeLimit(3600);
		try { 
			jt.setNativeSpecification("-l walltime=1:00:00");
		} catch (Exception e) {
			System.out.println("Failed to set the user");
		}

		/*jt.setWorkingDirectory("$drmaa_hd_ph$");
		//jt.setRemoteCommand(jobPath);
		jt.setArgs(Collections.singletonList(Integer.toString(seconds)));
		jt.setJoinFiles(true);
		jt.setInputPath("localhost:/home/vagrant");
		 */

		if (isBulkJob) {
		  //jt.setOutputPath("vm-cluster-client:$drmaa_hd_ph$" + "/BULKJOB" + "$drmaa_incr_ph$");
		  System.out.println(jt.getOutputPath());
		}

		FileTransferMode mode = new FileTransferMode(true, true, true);
		try {
			jt.setTransferFiles(mode);
		} catch (Exception e) {
			/* Don't care. */
			System.err.println(e);
		}

		try {
			FileTransferMode retMode = jt.getTransferFiles();
		} catch (Exception e) {
			/* Don't care. */
			System.err.println(e);
		}

		return jt;
	}
}
