# PBS Java DRMAA Library

This is a PBS compatible port of the Java DRMAA library originally developed for the Sun Grid Engine. The source was ported from https://svn.code.sf.net/p/gridscheduler/code/trunk/source/libs/jdrmaa/ to run against PBS/Torque/MOAB. It has been tested against Torque v5.0.0 (https://github.com/adaptivecomputing/torque.git), and the pbs-drmaa v0.1.19 DRMAA driver. 

author: Evan Bollig

email: bollig@gmail.com

date: 5/13/2014

## Dependencies

1. Torque 
    * torque-client (CentOS)
    * libtorque.so and other dev headers/libraries enabled
    * DRMAA extensions enabled 
2. pbs-drmaa 0.1.19 or later (http://sourceforge.net/projects/pbspro-drmaa/)
3. Java JNI
    * java-1.8.0-openjdk.x86_64 (CentOS)
4. Ant 
5. build-essential (gcc/g++)

## How to use this software

### To Build

Use ant to build all libraries
  > user$ ant

This will produce CLASSES/jdrmaa/libjdrmaa.so and CLASSES/jdrmaa/drmaa.jar. 

### To Install

* Add the libjdrmaa.so to the linking path. Optionally: 
  > root# cp CLASSES/jdrmaa/libjdrmaa.so /usr/lib/.

* Add the drmaa.jar to your Java application. I personally prefer to save the jar to my Maven repository: 
  > (OPTIONAL) user$ mvn install:install-file -DgroupId=jdrmaa -DartifactId=pbs-jdrmaa -Dversion=0.0.1 -Dpackaging=jar -Dfile=/tmp/pbs-jdrmaa/CLASSES/jdrmaa/drmaa.jar 

### To Test Job Submissions

* This will submit a handful of jobs and confirm that your DRMAA installation is functioning: 
  > user$ ant run.DrmaaExample 

* Confirm with: 
  > user$ qstat -u $USER

### To Unit Test

Unit testing depends on the presence of the junit.jar

* Build the test jar 
  > user$ ant test.all

* Run the unit test suite
  > user$ java -Djava.library.path=CLASSES/jdrmaa/. -cp CLASSES/jdrmaa/drmaa-test.jar:CLASSES/jdrmaa/drmaa.jar:$HOME/.m2/repository/junit/junit/4.10/junit-4.10.jar:. org.junit.runner.JUnitCore DrmaaSuite
